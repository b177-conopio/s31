const express = require("express");
// Create a Router instance that functions as a routing system
const router = express.Router();
// Import the taskControllers
const taskController = require("../controllers/taskControllers");

// Route to get all tasks
// endpoint: localhost:3001/tasks/
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// Route to create a task
// endpoint: localhost:3001/tasks/
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to delete task
// .delete is http model
// endpoint: localhost:3001/tasks/123
// req.params.id (id is the parameter needed to find) params property
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route to update task
// req.body will handle new value to update task
// (req.params.id, req.body) are arguments
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

// ACTIVITY
// Route to find specific task
router.get("/:id", (req, res) => {
	taskController.getOneTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

// Create a route for changing the status of a task to "complete"
router.put("/:id/complete", (req, res) => {
	taskController.updateStatus(req.params.id, req.params.status).then(
		resultFromController => res.send(resultFromController))
})


// export the router object to be used in index.js
module.exports = router;