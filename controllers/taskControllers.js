// To import model from models folder
const Task = require("../models/task");

// Controller function for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

// Controller function for creating a task
module.exports.createTask = (requestBody) => {
	// Create a task object based on the mongoose model "Task"
	let newTask = new Task({
		// Sets the "name" property with the value received from the client
		name: requestBody.name
	})

	// save new data
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		}
		// Save is successful
		else{
			return task;
		}
	})
}

// Controller function for deleting a task
// Assign name to function "deleteTask"
// taskId parameter came from data from client
// removedTask and err are user-defined parameters
// findByIdAndRemove is a mongoose model

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			return removedTask;
		}
	})
}

// Controller function for updating a task
// newContent is the new detail for the taskId 
// findById(taskId) = taskId is parameter
// reassign name from newContent to result name
// save result after reassigning
// console log error for the terminal
// return false for postman
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false
		}

		result.name = newContent.name

		// Saves the updated result in the MongoDB database
		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updatedTask;
			}
		})
	})
}

// ACTIVITY
// Get a specific task
module.exports.getOneTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
}

// Create a controller function for changing the status of a task to "complete".
module.exports.updateStatus = (taskId, newStatus) => {
	return Task.findById(taskId).then((result, error) => {
		if (error){
			console.log(error);
			return false;
		}

		result.status = "complete"

		return result.save().then((updatedStatus, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updatedStatus;
			}
		})

	})
}