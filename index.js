// Set up the dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 3001; 
// to read JSON data
app.use(express.json());
// to read forms data
app.use(express.urlencoded({extended:true}));

// Database connection
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.de5yx.mongodb.net/b177-to-do?retryWrites=true&w=majority",{
	// objects to evoid error on connecting to mongoDB
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// base URL "tasks"
app.use("/tasks", taskRoute)

// Server listening
app.listen(port, () => console.log(`Server running at port ${port}`));